#import <Foundation/Foundation.h>

int main() {

  __block NSString* test = @"foo";

  @synchronized(test) {
    NSLog(@"in the outer synchronized, test is %@", test);
    int64_t delta = (int64_t)(1.0e9 * 0.3f);
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, delta), dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
      @synchronized(test) {
        test = @"baz";
        NSLog(@"block fired, test set to %@", test);
      }
    });
  }

  while (true) {
    @synchronized(test) {
      if ([test isEqualToString:@"baz"]) {
        NSLog(@"while loop detected that test was set to %@, breaking", test);
        break;
      } else {
        test = @"bar";
        NSLog(@"while loop did not detect correct value for test, setting to %@", test);
      }
    }
  }

  return 0;
}
