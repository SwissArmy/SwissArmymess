keep n x
    | x >= n    = [x]
    | otherwise = []

dup n x
    | n == 1    = [x]
    | otherwise = [x] ++ dup (n-1) x

eatchoc (n:c:m:[]) = (n `div` c) + ((n `div` c) `div` m)

oddsum n
    | mod n 2 == 0 = 0
    | otherwise    = n

skip :: [a] -> [a]
skip []       = []
skip (x:[])   = []
skip (x:y:[]) = y:[]
skip (x:y:xs) = y:(skip xs)

rev :: [a] -> [a]
rev []     = []
rev (x:xs) = concat [rev xs, [x]]

hello_worlds 1 = putStrLn "Hello World!"
hello_worlds n = do
    putStrLn "Hello World!"
    hello_worlds (n-1)

fac n = product [1..n]

e :: Double -> Double -> Double
e 0 x = 1
e n x = (e (n-1) x) + ((x ** n) / (fac n))
